// Simple USBHost Logtigech C270 for FRDM-KL46Z
#include "USBHost.h"
#include "BaseUvc.h"
#include "decodeMJPEG.h"

// Logitech C270
#define C270_VID 0x046d
#define C270_PID 0x0825
#define C270_160x120 2
#define C270_176x144 3
#define C270_320x176 4
#define C270_320x240 5
#define C270_352x288 6
#define C270_432x240 7
#define C270_640x480 1
#define C270_544x288 8
#define C270_640x360 9
#define C270_752x416 10
#define C270_800x448 11
#define C270_800x600 12

#define C270_MJPEG 2
#define C270_YUV2  1

#define C270_EN  0x81
#define C270_MPS  192
#define C270_IF_ALT 1

class USBHostC270 : public BaseUvc, public decodeMJPEG {
public:
    USBHostC270(int formatIndex = C270_MJPEG, int frameIndex = C270_160x120, uint32_t interval = _5FPS);
    /**
     * read jpeg image
     *
     * @param buf read buffer 
     * @param size buffer size 
     * @param timeout_ms timeout default 15sec
     * @return jpeg size if read success else -1
     */
    int readJPEG(uint8_t* buf, int size, int timeout_ms = 15*1000);

    Report* report;

private:
    int _formatIndex;
    int _frameIndex;
    uint32_t _interval;
    uint8_t _seq;
    uint8_t* _buf;
    int _pos;
    int _size;

    void setup();
    virtual void outputJPEG(uint8_t c, int status); // from decodeMJPEG
    void callback_motion_jpeg(uint16_t frame, uint8_t* buf, int len);
};

