#include "USBHostC270.h"
//#include "dbg.h"

#define C270_DEBUG 1
#ifdef C270_DEBUG
#define C270_DBG(x, ...) std::printf("[%s:%d]"x"\r\n", __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define C270_DBG(...)  while(0);
#endif

USBHostC270::USBHostC270(int formatIndex, int frameIndex, uint32_t interval) {
    _formatIndex = formatIndex;
    _frameIndex = frameIndex;
    _interval = interval;
    clearOnResult();
    host = USBHost::getHostInst();
    dev = host->getDevice(0);
    report = &host->report;
    setup();
}

void USBHostC270::setup() {
    ep_iso_in->setAddress(C270_EN);
    ep_iso_in->setSize(C270_MPS);
    uint8_t buf[26];
    memset(buf, 0, sizeof(buf));
    buf[2] = _formatIndex;
    buf[3] = _frameIndex;
    *reinterpret_cast<uint32_t*>(buf+4) = _interval;
    USB_TYPE res = Control(SET_CUR, VS_COMMIT_CONTROL, 1, buf, sizeof(buf));
    if (res != USB_TYPE_OK) {
         C270_DBG("SET_CUR VS_COMMIT_CONTROL FAILED");
    }
    res = setInterfaceAlternate(1, C270_IF_ALT); // alt=1 packet size = 192
    if (res != USB_TYPE_OK) {
         C270_DBG("SET_INTERFACE FAILED");
    }
}

#define SEQ_READ_IDOL 0
#define SEQ_READ_EXEC 1
#define SEQ_READ_DONE 2

int USBHostC270::readJPEG(uint8_t* buf, int size, int timeout_ms) {
    _buf = buf;
    _pos = 0;
    _size = size;
    _seq = SEQ_READ_IDOL;
    setOnResult(this, &USBHostC270::callback_motion_jpeg);
    Timer timeout_t;
    timeout_t.reset();
    timeout_t.start();
    while(timeout_t.read_ms() < timeout_ms && _seq != SEQ_READ_DONE) {
        poll();
    } 
    return _pos;
}

/* virtual */ void USBHostC270::outputJPEG(uint8_t c, int status) { // from decodeMJPEG
    if (_seq == SEQ_READ_IDOL) {
        if (status == JPEG_START) {
            _pos = 0;
            _seq = SEQ_READ_EXEC;
        }
    }
    if (_seq == SEQ_READ_EXEC) {
        if (_pos < _size) {
            _buf[_pos++] = c;  
        }  
        if (status == JPEG_END) {
            _seq = SEQ_READ_DONE;
        }
    }
}

void USBHostC270::callback_motion_jpeg(uint16_t frame, uint8_t* buf, int len) {
        inputPacket(buf, len);
}
