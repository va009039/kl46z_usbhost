#ifdef TEST_USBHOST_MOUSE
// Simple USBHost Mouse for FRDM-KL46Z test program

#include "USBHostMouse.h"

DigitalOut led1(PTD5); // green
DigitalOut led2(PTE29); // red
#define LED_OFF 1
#define LED_ON  0

#if 1

void callback(uint8_t buttons, int8_t x, int8_t y, int8_t z) {
    led2 = buttons ? LED_ON : LED_OFF; // button on/off
    printf("%02x %d %d %d\n", buttons, x, y, z);
}

void callback_buttons(uint8_t buttons) {
    led2 = buttons ? LED_ON : LED_OFF; // button on/off
    printf("%02x\n", buttons);
}

int main() {
    USBHostMouse mouse;

    if (!mouse.connect()) {
        error("USB mouse not found.\n");
    }
    //mouse.attachEvent(callback);
    mouse.attachButtonEvent(callback_buttons);
    led2 = LED_OFF;
    Timer t;
    t.start();
    while(1) {
        USBHost::poll();
        wait_ms(10);
        if (t.read_ms() > 200) {
            led1 = !led1;
            t.reset();
        }
    }
}

#else
int main() {
    USBHostMouse mouse;
    mouse.report->clear();
    mouse.report->print_errstat();
    led2 = LED_OFF;
    Timer t;
    for(int n = 0;;) {
        uint8_t data[4];
        int result = mouse.readReport(data);
        if (result > 0) {
            led2 = data[0] ? LED_ON : LED_OFF; // button on/off
            printf("%d %02x %02x %02x %02x\r\n", n++, data[0], data[1], data[2], data[3]);
        }
        led1 = !led1;
        wait_ms(20);
        if (t.read_ms() > 1000*15) {
            mouse.report->print_errstat();
            t.reset();
        }
    }
}
#endif
#endif
