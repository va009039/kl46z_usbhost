#ifdef TEST_USBHOST_CAM
// Simple USBHost Cam for FRDM-KL46Z test program
#define USE_XMODEM

#include "USBHostCam.h"
Serial pc(USBTX, USBRX);
DigitalOut led1(PTD5); // green
DigitalOut led2(PTE29); // red
#define LED_OFF 1
#define LED_ON  0

uint8_t image_buf[1024*8];

#if 1
void print_hex(uint8_t* data, int size) {
    for(int i = 0; i < size; i++) {
        printf("%02X", data[i]);
        if (i % 32 == 31) {
            printf("\n");
        }
    }
    printf("\n\n");
}

#ifdef USE_XMODEM
int main() {
    pc.baud(9600);
    led1 = led2 = LED_OFF;

    // Logitech C270
    USBHostCam* cam = new USBHostCam(_320x240);
    if (!cam->connect()) {
        error("WebCam not found.\n");
    }
    int pos = 0; 
    int size = 0;
    Timer t;
    for(int seq = 0;;) {
        int c = -1;
        if (pc.readable()) {
            c = pc.getc();
        }    
        switch(seq) {
            case 0:
                pc.printf("\nReady, please download JPEG using XMODEM from FRDM-KL46Z.\n");
                t.start();
                seq++;
                break;
            case 1:
                if (c == 0x15) { // NAK
                    size = cam->readJPEG(image_buf, sizeof(image_buf));
                    pos = 0;
                    seq++;
                }
                break;
            case 2:
                if (pos >= size) {
                    pc.putc(0x04); // EOT
                } else {
                    pc.putc(0x01); // SOH
                    uint8_t block = pos/128 + 1;
                    pc.putc(block);
                    pc.putc(block ^ 0xff);
                    uint8_t checksum = 0x00;
                    for(int i = 0; i < 128; i++) {
                        uint8_t c = image_buf[pos + i];
                        checksum += c;
                        pc.putc(c);
                    }
                    pc.putc(checksum);
                }
                seq++;
                t.reset();
                led2 = !led2;
                break;
            case 3:
                if (c == 0x06) { // ACK
                    if (pos >= size) {
                        seq = 0;
                    } else {
                        pos += 128;
                        seq--;
                    }
                }
                break;
        }
        if (t.read_ms() > 15*1000) { // timeout
            t.reset();
            seq = 0;
        }
        led1 = !led1;
        cam->poll();
    }
}

#else
int main() {
    pc.baud(921600);
    led1 = led2 = LED_OFF;

    // Logitech C270
    USBHostCam* cam = new USBHostCam(_160x120); 


    Timer interval_t;
    interval_t.start();
    int shot = 0;
    while(1) {
        if (interval_t.read_ms() > 5*1000) {
            int r = cam->readJPEG(image_buf, sizeof(image_buf));
            print_hex(image_buf, r);
            printf("shot: %d, JPEG size: %d bytes\n", ++shot, r);
            interval_t.reset();
            led1 = !led1;
        }
        cam->poll();
    }
}
#endif

#else

USBHostCam* cam;
uint8_t  image[1024*3];
int image_pos = 0;
int image_count = 0;

void callback(uint16_t frame, uint8_t* buf, int len) {
    if (len >= 12) {
        for(int i = 12; i < len && image_pos < sizeof(image); i++) {
            image[image_pos++] = buf[i];
        }
        uint8_t bfh = buf[1];
        if (bfh & 0x40) { // ERR
            led2 = !led2;
        }    
        if (bfh & 0x02) { // EOF
            image_count++;
            led1 = !led1;
            printf("%d %d\n", image_count, image_pos);
            if (image_count % 20 == 0) {
                for(int i = 0; i < image_pos; i++) {
                    printf("%02x%c", image[i], (i % 32 == 31) ? '\n' : ' ');
                }
                printf("\n\n");
            }
            image_pos = 0;
        }
    }    
}

int main() {
    pc.baud(921600);

    led1 = LED_OFF;
    led2 = LED_OFF;
    cam = new USBHostCam(_160x120);
    cam->setOnResult(callback);
    while(1) {
        cam->poll();
    }
}
#endif

#endif // TEST_USBHOST_C270

