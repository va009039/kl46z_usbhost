#ifdef TEST_MOUSE_GPS_RSSI
#include "USBHostGPS.h"
#include "USBHostMouse.h"
#include "USBHostRSSI.h"

DigitalOut led1(PTD5); // green
DigitalOut led2(PTE29); // red
#define LED_OFF 1
#define LED_ON  0

void callback_buttons(uint8_t buttons) {
    led2 = buttons ? LED_ON : LED_OFF; // button on/off
    printf("\n[USB Mouse %02x]\n", buttons);
}

void callback_gps(char* buf, int size) {
    static uint8_t cksum;
    for(int i = 0; i < size; i++) {
        char c = buf[i];
        printf("%c", c);
        if (c == '$') {
            cksum = 0;
        } else if (c == '*') {
#ifdef _USB_DBG
            printf("[checksum=%02X]", cksum);
#endif
            } else {
            cksum ^= c;
        }
        led1 = !led1;
    }
}

void callback_bt(inquiry_with_rssi_info* info) {
    static int n = 0;
    char buf[18];
    info->bdaddr.str(buf, sizeof(buf));
    printf("%d %s %d\n", n++, buf, info->rssi);
}

int main() {
    // GT-730F/L 38400bps
    // Gosget SD-200 GPS DONGLE 4800bps
#ifdef GT730
    USBHostGPS gps(38400);
#else
    USBHostGPS gps(4800);
#endif
    if (!gps.connect()) {
        error("GPS not found.\n");
    }
    gps.attachEventRaw(callback_gps);

    USBHostMouse mouse;
    if (!mouse.connect()) {
        error("USB mouse not found.\n");
    }
    mouse.attachButtonEvent(callback_buttons);

    USBHostRSSI bt;
    if (!bt.connect()) {
        error("Bluetooth not found.\n");
    }
    bt.attachEvent(callback_bt);

    while(1) {
        USBHost::poll();
    }
}

#endif // TEST_MOUSE_GPS_RSSI
