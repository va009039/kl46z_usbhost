#ifdef TEST_USBHOST_GPS
// Simple USBHost GPS Dongle for FRDM-KL46Z test program
#include "USBHostGPS.h"
#ifdef _SLCD
#include "SLCD.h"
SLCD slcd;
#endif
DigitalOut led1(PTD5); // green
DigitalOut led2(PTE29); // red
#define LED_OFF 1
#define LED_ON  0

#if 1
void callbackRaw(char* buf, int size) {
    static uint8_t cksum;
    for(int i = 0; i < size; i++) {
        char c = buf[i];
        printf("%c", c);
        if (c == '$') {
            cksum = 0;
        } else if (c == '*') {
#ifdef _USB_DBG
            printf("[checksum=%02X]", cksum);
#endif
            } else {
            cksum ^= c;
        }
        led2 = !led2;
    }
}

int main() {
    // GT-730F/L 38400bps
    // Gosget SD-200 GPS DONGLE 4800bps
#ifdef GT730
    USBHostGPS gps(38400);
#else
    USBHostGPS gps(4800);
#endif
    if (!gps.connect()) {
        error("GPS not found.\n");
    }
    gps.attachEventRaw(callbackRaw);
    Timer t;
    t.start();
    time_t prev = gps.nmea.update_t;
    while(1) {
        USBHost::poll();
        if (t.read_ms() > 200) {
            led1 = !led1;
            t.reset();
            if (prev != gps.nmea.update_t) {
                struct tm * timeinfo;
                timeinfo = localtime(&gps.nmea.update_t);
                char buf[5];
                strftime(buf, sizeof(buf), "%M%S", timeinfo);
                prev = gps.nmea.update_t;
#ifdef _SLCD
                slcd.printf("%.4s", buf);
#else
                printf("\n\n[SLCD=%.4s]\n\n", buf);
#endif                
            }
        }
    }
}
#else
int main() {
    // GT-730F/L 38400bps
    // Gosget SD-200 GPS DONGLE 4800bps
    USBHostGPS gps(4800);
    led2 = LED_OFF;
    while(1) {
        char buf[64];
        int result = gps.readNMEA(buf, sizeof(buf), 100);
        if (result > 0) {
            for(int i = 0; i < result; i++) {
                printf("%c", buf[i]);
            }
            led1 = !led1;
        }
    }
}
#endif

#endif
