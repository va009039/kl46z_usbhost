#ifdef TEST_USBHOST_RSSI
// Simple USBHost Bluetooth RSSI for FRDM-KL46Z test program

#include "USBHostRSSI.h"

DigitalOut led1(PTD5); // green
DigitalOut led2(PTE29); // red
#define LED_OFF 1
#define LED_ON  0

void callback(inquiry_with_rssi_info* info) {
    static int n = 0;
    char buf[18];
    info->bdaddr.str(buf, sizeof(buf));
    printf("%d %s %d\n", n++, buf, info->rssi);
    led2 = !led2;
}

int main() {
    USBHostRSSI bt;
    if (!bt.connect()) {
        error("Bluetooth not found.\n");
    }
    bt.attachEvent(callback);
    led2 = LED_OFF;
    Timer t;
    t.start();
    while(1) {
        USBHost::poll();
        wait_ms(10);
        if (t.read_ms() > 200) {
            led1 = !led1;
            t.reset();
        }
    }
}

#endif
