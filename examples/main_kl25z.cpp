#ifdef TEST_USBHOST_KL25Z
// Simple USBHost Mouse for FRDM-KL46Z test program

#include "USBHostKL25Z.h"

DigitalOut led1(PTD5); // green
DigitalOut led2(PTE29); // red
#define LED_OFF 1
#define LED_ON  0

void int_callback(uint8_t* data, int size) {
    if (size > 0) {
        printf("len=%d\n", size);
        for(int i = 0; i < size; i++) {
            printf("%02x ", data[i]);
        }
        printf("\n");
    }
    led2 = !led2;
}

int main() {
    USBHostKL25Z kl25z;

    if (!kl25z.connect()) {
        error("FRDM-KL25Z not found.\n");
    }
    kl25z.int_attachEvent(int_callback);
    led2 = LED_OFF;
    Timer t;
    t.start();
    int size = 0;
    while(1) {
        USBHost::poll();
        wait_ms(100);
        if (t.read_ms() > 900) {
            led1 = !led1;
            t.reset();
            uint8_t data[size];
            for(int i = 0; i < size; i++) {
                data[i] = rand() & 0xff;
            }
            kl25z.send_report(data, size);
            size = (size + 1) % 64;
        }
    }
}

#endif
