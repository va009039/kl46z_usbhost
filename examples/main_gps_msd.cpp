#ifdef TEST_USBHOST_GPS_MSD
// Simple USBHost mouse and GPS Dongle for FRDM-KL46Z test program
#include "USBHostGPS.h"
#include "USBHostMSD.h"

DigitalOut led1(PTD5); // green
DigitalOut led2(PTE29); // red
#define LED_OFF 1
#define LED_ON  0

FILE* fp = NULL;
int count = 0;

void callback_gps(char* buf, int size) {
    for(int i = 0; i < size; i++) {
        char c = buf[i];
        printf("%c", c);
        if (fp) {
            fprintf(fp, "%c", c);
            count++;
        }
        led1 = !led1;
    }
}

int main() {
    USBHostMSD msd("usb");
    if (!msd.connect()) {
        error("MSD not found.\n");
    }
    // GT-730F/L 38400bps
    // Gosget SD-200 GPS DONGLE 4800bps
#ifdef GT730
    USBHostGPS gps(38400);
#else
    USBHostGPS gps(4800);
#endif
    if (!gps.connect()) {
        error("GPS not found.\n");
    }
    gps.attachEventRaw(callback_gps);

    int n = 0;
    while(1) {
        if (fp == NULL || count > 100000) {
            if (fp) {
                fclose(fp);
            }
            char buf[64];
            snprintf(buf, sizeof(buf), "/usb/gps%02d.txt", n++ % 20);
            printf("\n\n%s\n\n", buf);
            fp = fopen(buf, "w");
            if (fp == NULL) {
                error("file open error.\n");
            }
            count = 0;
            led2 = !led2;
        }
        USBHost::poll();
    }
}

#endif // TEST_USBHOST_GPS_MSD
