/* mbed USBHost Library
 * Copyright (c) 2006-2013 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "USBHostConf.h"

#include "USBHost.h"

/** 
 * A class to communicate a USB mouse
 */
class USBHostKL25Z : public IUSBEnumerator {
public:

    /**
    * Constructor
    */
    USBHostKL25Z();

    /**
     * Try to connect a mouse device
     *
     * @return true if connection was successful
     */
    bool connect();

    /**
    * Check if a mouse is connected
    *
    * @returns true if a mouse is connected
    */
    bool connected();

    void int_attachEvent(void (*ptr)(uint8_t* data, int size)) {
        if (ptr != NULL) {
            int_onUpdate = ptr;
        }
    }

    void bulk_attachEvent(void (*ptr)(uint8_t* data, int size)) {
        if (ptr != NULL) {
            bulk_onUpdate = ptr;
        }
    }

    void send_report(uint8_t* data, int size) {
        if (int_out) {
            host->interruptWrite(dev, int_out, data, size);
        }
    }

protected:
    //From IUSBEnumerator
    virtual void setVidPid(uint16_t vid, uint16_t pid);
    virtual bool parseInterface(uint8_t intf_nb, uint8_t intf_class, uint8_t intf_subclass, uint8_t intf_protocol); //Must return true if the interface should be parsed
    virtual bool useEndpoint(uint8_t intf_nb, ENDPOINT_TYPE type, ENDPOINT_DIRECTION dir); //Must return true if the endpoint will be used

private:
    USBHost * host;
    USBDeviceConnected * dev;
    USBEndpoint * int_in;
    USBEndpoint * int_out;
    USBEndpoint * bulk_in;
    USBEndpoint * bulk_out;
    uint8_t int_buf[64];
    uint8_t bulk_buf[64];
    
    bool dev_connected;
    bool kl25z_device_found;
    int kl25z_intf;

    void int_rxHandler();
    void bulk_rxHandler();
    void (*int_onUpdate)(uint8_t* data, int size);
    void (*bulk_onUpdate)(uint8_t* data, int size);
    int report_id;
    void init();
};
