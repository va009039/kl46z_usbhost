/* mbed USBHost Library
 * Copyright (c) 2006-2013 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "USBHostKL25Z.h"

USBHostKL25Z::USBHostKL25Z() {
    host = USBHost::getHostInst();
    init();
}

void USBHostKL25Z::init() {
    dev = NULL;
    int_in = NULL;
    int_out = NULL;
    bulk_in = NULL;
    bulk_out = NULL;
    int_onUpdate = NULL;
    bulk_onUpdate = NULL;
    report_id = 0;
    dev_connected = false;
    kl25z_device_found = false;
    kl25z_intf = -1;
}

bool USBHostKL25Z::connected() {
    return dev_connected;
}

bool USBHostKL25Z::connect() {

    if (dev_connected) {
        return true;
    }
    
    for (uint8_t i = 0; i < MAX_DEVICE_CONNECTED; i++) {
        if ((dev = host->getDevice(i)) != NULL) {
            if(host->enumerate(dev, this)) {
                break;
            }
            if (kl25z_device_found) {
                int_in = dev->getEndpoint(kl25z_intf, INTERRUPT_ENDPOINT, IN);
                int_out = dev->getEndpoint(kl25z_intf, INTERRUPT_ENDPOINT, OUT);
                bulk_in = dev->getEndpoint(kl25z_intf, BULK_ENDPOINT, IN);
                bulk_out = dev->getEndpoint(kl25z_intf, BULK_ENDPOINT, OUT);
                USB_INFO("New KL25Z device: VID:%04x PID:%04x [dev: %p - intf: %d]", dev->getVid(), dev->getPid(), dev, kl25z_intf);
                dev->setName("KL25Z", kl25z_intf);
                host->registerDriver(dev, kl25z_intf, this, &USBHostKL25Z::init);
                if(int_in) {
                    int_in->attach(this, &USBHostKL25Z::int_rxHandler);
                    host->interruptRead(dev, int_in, int_buf, int_in->getSize(), false);
                }
                if(bulk_in) {
                    bulk_in->attach(this, &USBHostKL25Z::bulk_rxHandler);
                    host->bulkRead(dev, bulk_in, bulk_buf, bulk_in->getSize(), false);
                }
                dev_connected = true;
                return true;
            }
        }
    }
    init();
    return false;
}

void USBHostKL25Z::int_rxHandler() {
    int len_listen = int_in->getSize();
    
    if (int_onUpdate) {
        (*int_onUpdate)(int_buf, int_in->getLengthTransferred());
    }
    if (dev) {
        host->interruptRead(dev, int_in, int_buf, len_listen, false);
    }
}

void USBHostKL25Z::bulk_rxHandler() {
    int len_listen = bulk_in->getSize();
    
    if (bulk_onUpdate) {
        (*bulk_onUpdate)(bulk_buf, bulk_in->getLengthTransferred());
    }
    if (dev) {
        host->bulkRead(dev, bulk_in, bulk_buf, len_listen, false);
    }
}

/*virtual*/ void USBHostKL25Z::setVidPid(uint16_t vid, uint16_t pid)
{
    if (vid == 0x1234 && pid == 0x0006) {
        kl25z_device_found = true;
    }
}

/*virtual*/ bool USBHostKL25Z::parseInterface(uint8_t intf_nb, uint8_t intf_class, uint8_t intf_subclass, uint8_t intf_protocol) //Must return true if the interface should be parsed
{
    if (kl25z_intf == -1 && intf_class == HID_CLASS) {
        kl25z_intf = intf_nb;
        return true;
    }
    return false;
}

/*virtual*/ bool USBHostKL25Z::useEndpoint(uint8_t intf_nb, ENDPOINT_TYPE type, ENDPOINT_DIRECTION dir) //Must return true if the endpoint will be used
{
    if (intf_nb == kl25z_intf) {
        if (type == INTERRUPT_ENDPOINT) {
            return true;
        }
    }
    return false;
}

