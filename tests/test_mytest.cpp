#ifdef TEST_MYTEST
// test_mytest.cpp 2013/6/28
#include "mbed.h"
#include <algorithm>
#include "mytest.h"

//Serial pc(USBTX, USBRX);

TEST(mytest,test1) {
    int a = 1;
    int b = 2;
    int c = a + b;
    ASSERT_TRUE(c == 3);
}

TEST(mytest,test2) {
    wait_ms(50);
    ASSERT_TRUE(1);
}

TEST(mytest,test3) {
    wait_ms(100);
    ASSERT_TRUE(1);
}

TEST(mytest,test4) {
    wait_ms(150);
    ASSERT_TRUE(1);
}

int main() {
    //pc.baud(921600);
    printf("%s", __FILE__);

    RUN_ALL_TESTS();
    exit(1);
}

#endif

