#ifdef TEST_MYMAP

#include "mbed.h"
#include "mytest.h"
#include "mymap.h"
#include "myvector.h"
#include "myqueue.h"

//Serial pc(USBTX, USBRX);

mymap<int,const char*>map1;

TEST(mymap,test1) {
    ASSERT_TRUE(map1.size() == 0);
    ASSERT_TRUE(map1.empty());
}

const char* v1 = "123";
TEST(mymap,test2) {
    map1[1] = v1;
    ASSERT_TRUE(map1.size() == 1);
    ASSERT_TRUE(map1.empty() == false);
    ASSERT_TRUE(map1[1] == v1);
    ASSERT_TRUE(map1.count(0) == 0);
    ASSERT_TRUE(map1.count(2) == 0);
}

const char* v2 = "456";
TEST(mymap,test3) {
    map1[2] = v2;
    ASSERT_TRUE(map1.size() == 2);
    ASSERT_TRUE(map1[1] == v1);
    ASSERT_TRUE(map1[2] == v2);
}

mymap<int,char*>map2;

TEST(mytest,test4) {
    for(int i = 0; i < 10; i++) {
        char* buf = new char[32];
        sprintf(buf, "%d", i);
        map2[i] = buf;
    }
    for(int i = 0; i < 10; i++) {
        int v = atoi(map2[i]);
        ASSERT_TRUE(i == v);
    }
}

myvector<int>vec1;

TEST(myvector,test1) {
    ASSERT_TRUE(vec1.size() == 0);
    ASSERT_TRUE(vec1.empty());
}

TEST(myvector,test2) {
    vec1.push_back(1);
    ASSERT_TRUE(vec1.size() == 1);
    ASSERT_TRUE(vec1.empty() == false);
}

myvector<int>vec2;
TEST(myvector,test3) {
    for(int i = 0; i < 10; i++) {
        vec2.push_back(i);
    }
    for(int i = 0; i < 10; i++) {
        ASSERT_TRUE(vec2[i] == i);
    }
}

myqueue<int>q1;

TEST(myqueue,test1) {
    ASSERT_TRUE(q1.size() == 0);
    q1.push(1);
    ASSERT_TRUE(q1.size() == 1);
    q1.push(2);
    ASSERT_TRUE(q1.size() == 2);
    int v1 = q1.pop();
    ASSERT_TRUE(q1.size() == 1);
    ASSERT_TRUE(v1 == 1);
    int v2 = q1.pop();
    ASSERT_TRUE(q1.size() == 0);
    ASSERT_TRUE(v2 == 2);
}

myqueue<int>q2;

TEST(myqueue,test2) {
    ASSERT_TRUE(q2.size() == 0);
    for(int i = 0; i < 10; i++) {
        q2.push(i);
        ASSERT_TRUE(q2.size() == (i+1));
    }
    for(int i = 0; i < 10; i++) {
        int v = q2.pop();
        ASSERT_TRUE(i == v);
    }
}

myqueue<int>q3;

TEST(myqueue,test3) {
    int i = 0;
    int j = 0;
    for(; i <= 20; i++) {
        q3.push(i);
    }
    for(; j <= 5; j++) {
        int v = q3.pop();
        ASSERT_TRUE(v == j);
    }
    for(; i <= 30; i++) {
        q3.push(i);
    }
    for(; j <= 25; j++) {
        int v = q3.pop();
        ASSERT_TRUE(v == j);
    }
}

int main() {
    //pc.baud(921600);
    printf("%s", __FILE__);

    RUN_ALL_TESTS();
    exit(1);
}

#endif
